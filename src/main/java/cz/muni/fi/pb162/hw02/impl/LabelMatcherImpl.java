package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.HasLabels;
import cz.muni.fi.pb162.hw02.LabelMatcher;

import java.util.Set;

/**
 * Implementation of the LabelMatcher interface
 *
 * @author Matej Drlik
 */

public class LabelMatcherImpl implements LabelMatcher {
    private final ExpressionMatcher matcher;

    /**
     * Constructor
     *
     * @param matcher .
     */
    public LabelMatcherImpl(ExpressionMatcher matcher) {
        this.matcher = matcher;
    }

    @Override
    public boolean matches(HasLabels labeled) {
        Set<String> labels = labeled.getLabels();
        StringBuilder labelString = new StringBuilder();
        for (String label : labels){
            labelString.append(label);
        }
        String completed = labelString.toString();
        return matcher.matches(completed);
    }

    @Override
    public boolean all(Iterable<HasLabels> labeled) {
        for (HasLabels currentLabel : labeled) {
            if (!this.matches(currentLabel)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean any(Iterable<HasLabels> labeled) {
        for (HasLabels currentLabel : labeled) {
             if (this.matches(currentLabel)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean none(Iterable<HasLabels> labeled) {
        return !this.any(labeled);
    }
}
