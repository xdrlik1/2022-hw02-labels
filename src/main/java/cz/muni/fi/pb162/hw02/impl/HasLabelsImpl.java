package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.HasLabels;

import java.util.Objects;
import java.util.Set;

/**
 * Implementation of the HasLabels interface
 *
 * @author Matej Drlik
 */

public class HasLabelsImpl implements HasLabels {
    private final Set<String> labels;
    private final String title;

    /**
     * Constructor
     * @param labels .
     * @param title .
     */

    public HasLabelsImpl(Set<String> labels, String title) {
        Objects.requireNonNull(title, "Title should not be null");
        Objects.requireNonNull(labels, "Labels should not be null");

        this.labels = Set.copyOf(labels);
        this.title = title;
    }

    @Override
    public Set<String> getLabels() {
        return labels;
    }
}
