package cz.muni.fi.pb162.hw02.impl;

/**
 * Class for representation of the expression with '&' operator.
 *
 * @author Matej Drlik
 */

public class AndExpression implements ExpressionMatcher{
    private ExpressionMatcher leftExpression;
    private ExpressionMatcher rightExpression;

    /**
     * Constructor
     * @param leftExpression .
     * @param rightExpression .
     */
    public AndExpression(ExpressionMatcher leftExpression, ExpressionMatcher rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public boolean matches(String label) {
        return leftExpression.matches(label) && rightExpression.matches(label);
    }
}
