package cz.muni.fi.pb162.hw02.impl;

/**
 * Class for representation of one single element in the expression
 *
 * @author Matej Drlik
 */

public class SingleExpression implements ExpressionMatcher{
    private final String expression;

    /**
     * Constructor
     * @param expression String expression
     */
    public SingleExpression(String expression){
        this.expression = expression;
    }

    @Override
    public boolean matches(String label){
        return label.contains(expression);
    }
}
