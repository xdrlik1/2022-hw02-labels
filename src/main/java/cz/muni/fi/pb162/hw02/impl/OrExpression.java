package cz.muni.fi.pb162.hw02.impl;

/**
 * Class for representation of the expression with '|' operator,
 * aka OR expression
 *
 * @author Matej Drlik
 */

public class OrExpression implements ExpressionMatcher{
    private ExpressionMatcher leftExpression;
    private ExpressionMatcher rightExpression;

    /**
     * Constructor
     * @param leftExpression .
     * @param rightExpression .
     */
    public OrExpression(ExpressionMatcher leftExpression, ExpressionMatcher rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public boolean matches(String label) {
        return leftExpression.matches(label) || rightExpression.matches(label);
    }
}
