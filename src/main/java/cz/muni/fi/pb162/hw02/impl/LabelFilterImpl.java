package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.HasLabels;
import cz.muni.fi.pb162.hw02.LabelFilter;
import cz.muni.fi.pb162.hw02.impl.utils.SetUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the LabelFilter interface.
 *
 * @author Matej Drlik
 */

public class LabelFilterImpl implements LabelFilter {
    private final LabelMatcherImpl matcher;

    /**
     * Constructor
     * @param matcher .
     */
    public LabelFilterImpl(ExpressionMatcher matcher) {
        this.matcher = new LabelMatcherImpl(matcher);
    }

    @Override
    public Collection<HasLabels> matching(Iterable<HasLabels> labeled) {
        HashSet<HasLabels> filtered = new HashSet<>();

        for(HasLabels label : labeled){
            if(matcher.matches(label)){
                filtered.add(label);
            }
        }

        return filtered;
    }

    @Override
    public Collection<HasLabels> notMatching(Iterable<HasLabels> labeled) {
        Collection<HasLabels> filtered = new HashSet<>();
        for (HasLabels label : labeled){
            filtered.add(label);
        }
        filtered.removeAll(matching(labeled));
        return filtered;
    }

    @Override
    public Collection<HasLabels> joined(Iterable<HasLabels> fst, Iterable<HasLabels> snd) {
        Set<HasLabels> first = new HashSet<>(matching(fst));
        Set<HasLabels> second = new HashSet<>(matching(snd));

        return SetUtils.union(first, second);
    }

    @Override
    public Collection<HasLabels> distinct(Iterable<HasLabels> fst, Iterable<HasLabels> snd) {
        Set<HasLabels> filteredFst = new HashSet<>(matching(fst));
        filteredFst.removeAll(matching(snd));
        Set<HasLabels> filteredSnd = new HashSet<>(matching(snd));
        filteredSnd.removeAll(matching(fst));
        return SetUtils.union(filteredFst, filteredSnd);
    }

    @Override
    public Collection<HasLabels> intersection(Iterable<HasLabels> fst, Iterable<HasLabels> snd) {
        Collection<HasLabels> filtered = new HashSet<>(joined(fst, snd));
        filtered.removeAll(distinct(fst,snd));
        return filtered;
    }
}
