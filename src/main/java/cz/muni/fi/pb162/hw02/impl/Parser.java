package cz.muni.fi.pb162.hw02.impl;

import cz.muni.fi.pb162.hw02.error.InvalidExpressionException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Class for processing the expression we need for comparison with the labels.
 *
 * @author Matej Drlik
 */
public class Parser {

    /**
     * The main method that processes the String and passes it as iterator to the semi-recursive function
     * It is kind of messy, but I was lacking time
     *
     * @param expression The String expression to parse
     * @return Expression that sets the match conditions
     */
    public ExpressionMatcher parse(String expression) {
        if (expression == null){
            throw new InvalidExpressionException("Invalid expression.");
        }
        List<String> tmp = Arrays.asList(expression.split(""));
        ArrayList<String> cut = new ArrayList<>(tmp);
        ArrayList<String> finalCut = new ArrayList<>();
        Iterator<String> iterator = cut.iterator();
        StringBuilder currentString = new StringBuilder();

        while (iterator.hasNext()){
            String iteratorCurrent = iterator.next();
            if(iteratorCurrent.isBlank()){
                continue;
            } else if (isAnd(iteratorCurrent) || isOr(iteratorCurrent)){
                String current = currentString.toString();
                finalCut.add(current);
                finalCut.add(iteratorCurrent);
                currentString = new StringBuilder();
            } else{
                currentString.append(iteratorCurrent);
            }
        }
        finalCut.add(currentString.toString());
        Iterator<String> iteratorToPass = finalCut.iterator();

        return parse(iteratorToPass);
    }

    /**
     * Semi-recursive method that goes through the expression and creates
     * the Matcher object.
     * @param iterator comes from the main method. It's a transformed String
     * @return The processed expression returned to the main method
     */
    private ExpressionMatcher parse (Iterator<String> iterator){
        ExpressionMatcher currentmatcher = null;

        while(iterator.hasNext()){
            String current = iterator.next();
            if (isAnd(current)){
                if(currentmatcher == null){
                    throw new IllegalArgumentException("Not time for conjunction.");
                }
                currentmatcher = new AndExpression(currentmatcher, getNext(iterator));
            } else if (isOr(current)){
                if(currentmatcher == null){
                    throw new IllegalArgumentException("Not time for conjunction.");
                }
                currentmatcher = new OrExpression(currentmatcher, getNext(iterator));
            } else if(isDoubleNeg(current)){
                currentmatcher = new SingleExpression(current.replace("!", ""));
            } else if (isNeg(current)){
                currentmatcher = new NegationExpression(current);
            } else{
                currentmatcher = new SingleExpression(current);
            }
        }

        return currentmatcher;
    }

    /**
     * Help method to get the next element in iterator
     * @param iterator iterator
     * @return next element
     */
    private ExpressionMatcher getNext (Iterator<String> iterator){
        String next = iterator.next();
        if (isNeg(next)){
            return new NegationExpression(next);
        }
        return new SingleExpression(next);
    }

    /**
     * Help function
     * @param current .
     * @return .
     */
    private boolean isAnd (String current){
        return current.equals("&");
    }

    /**
     * Help function
     * @param current .
     * @return .
     */
    private boolean isOr (String current){
        return current.equals("|");
    }

    /**
     * Help function
     * @param current .
     * @return .
     */
    private boolean isNeg (String current){
        return current.startsWith("!");
    }

    /**
     * Help function
     * @param current .
     * @return .
     */
    private boolean isDoubleNeg (String current){
        return current.startsWith("!!");
    }
}
