package cz.muni.fi.pb162.hw02.impl;

/**
 * Interface for each part of the expression
 *
 * @author Matej Drlik
 */
public interface ExpressionMatcher {

    /**
     * Each type of expression has its own evaluation of match
     *
     * @param label we compare with the expression
     * @return true if label matches the expression
     */
    boolean matches(String label);
}
