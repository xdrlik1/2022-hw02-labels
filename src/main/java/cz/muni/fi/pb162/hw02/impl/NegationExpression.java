package cz.muni.fi.pb162.hw02.impl;

/**
 * Class for representation of the labels with '!' operator (negated expressions)
 *
 * @author Matej Drlik
 */

public class NegationExpression implements ExpressionMatcher{
    private String expression;

    /**
     * Constructor
     * @param expression .
     */
    public NegationExpression(String expression){
        this.expression = expression.replace("!", "");
    }

    @Override
    public boolean matches(String label) {
        return !(label.contains(expression));
    }
}
